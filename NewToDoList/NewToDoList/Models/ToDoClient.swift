import Foundation
import SwiftHTTP
import Gloss

enum URLPath: String {
    case registraction = "http://localhost:8080/todo-server/user/signup"
    case login = "http://localhost:8080/todo-server/user/signin"
    case createTask = "http://localhost:8080/todo-server/task/insert"
    case updateTask = "http://localhost:8080/todo-server/task/update"
}

enum RestCode: Int {
    case success = 200
    case notModified = 304
    case serverError = 500
    case unauthorized = 401
    case forbidden = 403
}

class ToDoClient {
    static var instance = ToDoClient()
    
    func authorization(email: String, password: String, _ final: @escaping (checkResult) -> Void) {
        do {
            let _ = try HTTP.GET(URLPath.login.rawValue, parameters: ["email": email, "password": password]).start() { response in
                if let code = response.statusCode {
                    switch code {
                    case RestCode.success.rawValue:
                        do {
                            if let json = try JSONSerialization.jsonObject(with: response.data) as? [String: Any] {
                                if let user = MainUser.init(json: json) {
                                    mainUser = user
                                    final((true, "Success authorization"))
                                } else {
                                    final((false, "Ошибка приложения, просьба обратиться в тех. поддержку"))
                                }
                            }
                        } catch {
                            final((false, "Ошибка приложения, просьба обратиться в тех. поддержку"))
                        }
                    case RestCode.forbidden.rawValue:
                        final((false, "Неверный email или пароль"))
                    default:
                        print("Code: \(code) \(response.error)")
                        final((false, "Ошибка сервера, просьба обратиться в тех. поддержку"))
                    }
                } else {
                    final((false, "Неизвестная ошибка."))
                }
            }
        } catch {
            NSLog("Global error: \(error.localizedDescription)")
            final((false, "Ошибка сети, проверте подключение к интернету"))
        }
    }
    
    func registration(user: User, _ final: @escaping (checkResult) -> Void) {
        do {
            let _ = try HTTP.POST(URLPath.registraction.rawValue, parameters: user.toJSON(), requestSerializer: JSONParameterSerializer()).start() { response in
                if let code = response.statusCode {
                    switch code {
                    case RestCode.success.rawValue:
                        do {
                            if let json = try JSONSerialization.jsonObject(with: response.data) as? [String: Any] {
                                if let user = MainUser(json: json) {
                                    mainUser = user
                                    final((true, "Вы успешно зарегестрировались"))
                                } else {
                                    final((false, "Ошибка приложения. Неудалось распарсить ответ от сервера"))
                                }
                            }
                        } catch {
                            final((false, "Ошибка сервера. Неудалось преобразовать ответ от сервера"))
                        }
                    case RestCode.unauthorized.rawValue:
                        final((false, "Узаказнные логин или email уже заняты"))
                    case RestCode.serverError.rawValue:
                        final((false, "Ошибка сервера. Просьба обратиться в тех. поддержку"))
                    default:
                        final((false, "Неизвестная ошибка: \(code)"))
                    }
                } else {
                    final((false, "Ошибка приложения"))
                }
            }
        } catch {
            NSLog("Global error: \(error.localizedDescription)")
            final(checkResult(false, "Неизвестная ошибка: \(error.localizedDescription)"))
        }
    }
    
    func addTask(task: Task, final: @escaping (checkResult) -> Void) {
        let param = task.toJSON()
        do {
            let _ = try HTTP.POST("\(URLPath.createTask.rawValue)?token=\(mainUser!.token!)", parameters: param, requestSerializer: JSONParameterSerializer()).start() { response in
                if let code = response.statusCode {
                    switch code {
                    case RestCode.success.rawValue:
                        final((true, "Success"))
                    default:
                        final((false, "Неизвестная ошибка: \(code)"))
                    }
                } else {
                    final((false, "Неизвестная ошибка: \(response.error)"))
                }
            }
        } catch {
            final((false, "Неизвестная ошибка"))
        }
    }

    func updateTask(task: Task, final: @escaping (checkResult) -> Void) {
        let param = task.toJSON()
        print("\(URLPath.updateTask.rawValue)?token=\(mainUser!.token!)")
        do {
            try HTTP.POST("\(URLPath.updateTask.rawValue)?token=\(mainUser!.token!)", parameters: param, requestSerializer: JSONParameterSerializer()).start() {
                response in
                if let code = response.statusCode {
                    switch code {
                    case RestCode.success.rawValue:
                        final((true, "Success"))
                    default:
                        final((false, "Ошибка \(code)"))
                    }
                } else {
                    final((false, "Неизвестная ошибка: \(response.error)"))
                }
            }
        } catch {
            final((false, "Неизвестная ошибка"))
        }
    }

    private init(){
    }
}
