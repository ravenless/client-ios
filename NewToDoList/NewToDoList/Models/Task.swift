import Foundation
import Gloss

class Task: Glossy {
    
    var id: Int?
    var create: String?
    var event: String?
    var header: String?
    var body: String?
    var type: TaskType?
    var status: TaskStatus?
    var tags: [TaskTag]?
    
    required init?(json: JSON) {
        id = "id" <~~ json
        create = "create" <~~ json
        event = "event" <~~ json
        header = "header" <~~ json
        body = "body" <~~ json
        type = "taskType" <~~ json
        status = "taskStatus" <~~ json
        tags = "tags" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "userId" ~~> mainUser!.id,
            "id" ~~> id,
//            "create" ~~> self.create,
//            "event" ~~> self.event,
            "header" ~~> self.header,
            "body" ~~> self.body,
            "taskType" ~~> self.type,
            "taskStatus" ~~> self.status,
            "tags" ~~> self.tags
            ])
    }
    
    init(event: String?, header: String?, body: String?, type: TaskType?, status: TaskStatus?, tags: [TaskTag]?) {
        self.event = event
        self.header = header
        self.body = body
        self.type = type
        self.status = status
        self.tags = tags
    }
}
