import Foundation
import Gloss

class DBUser: Glossy {
    
    var id: Int?
    var token: String?
    var login: String?
    var password: String?
    var name: String?
    var email: String?
    var tasks: [Int: DBTask]?
    var allTags: [DBTaskTag]?
    
    required init?(json: JSON) {
        self.tasks = [Int: DBTask]()
        id = "id" <~~ json
        token = "token" <~~ json
        login = "login" <~~ json
        password = "password" <~~ json
        name = "name" <~~ json
        email = "email" <~~ json
        allTags = "tags" <~~ json
        let tmpTasks: [DBTask]? = "taskList" <~~ json
        if tmpTasks != nil {
            for tmpTask in tmpTasks! {
                self.tasks?[tmpTask.id!] = tmpTask
            }
        }
    }
    
    init(login: String, password: String, name: String, email: String) {
        self.login = login
        self.password = password
        self.name = name
        self.email = email
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "name" ~~> self.name,
            "login" ~~> self.login,
            "password" ~~> self.password,
            "email" ~~> self.email
            ])
        
    }
    
    func toString() -> String {
        return String("User: [login: \(self.login), password: \(self.password), name: \(self.name), email: \(self.email)" )
    }
}


