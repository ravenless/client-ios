import Gloss

class MainUser: User {
    var newTask: [Task]!
    var successTask: [Task]!
    var deleteTask: [Task]!
    
    required init?(json: JSON) {
        super.init(json: json)
        if let tasks = self.tasks {
            newTask = tasks.filter { $0.status?.id! == newTaskStatus.id! }
            successTask = tasks.filter { $0.status?.id == successTaskStatus.id }
            deleteTask = tasks.filter { $0.status?.id == deleteTaskStatus.id }
            
//            newTask = newTask ?? [Task]()
//            successTask = successTask ?? [Task]()
//            deleteTask = deleteTask ?? [Task]()
        }
    }
}
