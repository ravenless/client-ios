import Foundation
import Gloss

class User: Glossy {
    
    var id: Int?
    var token: String?
    var login: String?
    var password: String?
    var name: String?
    var email: String?
    var tasks: [Task]?
    var allTags: [TaskTag]?
    
    required init?(json: JSON) {
        id = "id" <~~ json
        token = "token" <~~ json
        login = "login" <~~ json
        password = "password" <~~ json
        name = "name" <~~ json
        email = "email" <~~ json
        tasks = "taskList" <~~ json
        allTags = "tags" <~~ json
    }
    
    init(login: String, password: String, name: String, email: String) {
        self.login = login
        self.password = password
        self.name = name
        self.email = email
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "name" ~~> self.name,
            "login" ~~> self.login,
            "password" ~~> self.password,
            "email" ~~> self.email
            ])
        
    }
    
    func toString() -> String {
        return String("User: [login: \(self.login), password: \(self.password), name: \(self.name), email: \(self.email)" )
    }
}


