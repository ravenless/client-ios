//
//  TaskStatus+CoreDataProperties.swift
//  NewToDoList
//
//  Created by Сергей on 05.03.17.
//  Copyright © 2017 Сергей. All rights reserved.
//

import Foundation
import CoreData


extension TaskStatus {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TaskStatus> {
        return NSFetchRequest<TaskStatus>(entityName: "TaskStatus");
    }

    @NSManaged public var name: String?
    @NSManaged public var tasks: Set<Task>?

}

// MARK: Generated accessors for tasks
extension TaskStatus {

    @objc(addTasksObject:)
    @NSManaged public func addToTasks(_ value: Task)

    @objc(removeTasksObject:)
    @NSManaged public func removeFromTasks(_ value: Task)

    @objc(addTasks:)
    @NSManaged public func addToTasks(_ values: Set<Task>)

    @objc(removeTasks:)
    @NSManaged public func removeFromTasks(_ values: Set<Task>)

}
