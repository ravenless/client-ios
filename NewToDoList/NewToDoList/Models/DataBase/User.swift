//
//  User+CoreDataClass.swift
//  NewToDoList
//
//  Created by Сергей on 05.03.17.
//  Copyright © 2017 Сергей. All rights reserved.
//

import Foundation
import CoreData


public class User: NSManagedObject {
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "TDL")
        container.loadPersistentStores() { desc, err in
            print("База данных \(desc)")
            if let error = err as NSError?{
                fatalError("Unresourses error: \(error), \(error.userInfo)")
            }
        }
        return container
    }()

}
