//
//  User+CoreDataProperties.swift
//  NewToDoList
//
//  Created by Сергей on 05.03.17.
//  Copyright © 2017 Сергей. All rights reserved.
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User");
    }

    @NSManaged public var login: String?
    @NSManaged public var password: String?
    @NSManaged public var email: String?
    @NSManaged public var name: String?
    @NSManaged public var tasks: Set<Task>?

}

// MARK: Generated accessors for tasks
extension User {

    @objc(addTasksObject:)
    @NSManaged public func addToTasks(_ value: Task)

    @objc(removeTasksObject:)
    @NSManaged public func removeFromTasks(_ value: Task)

    @objc(addTasks:)
    @NSManaged public func addToTasks(_ values: Set<Task>)

    @objc(removeTasks:)
    @NSManaged public func removeFromTasks(_ values: Set<Task>)

}
