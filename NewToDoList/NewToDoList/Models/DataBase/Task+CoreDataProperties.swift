//
//  Task+CoreDataProperties.swift
//  NewToDoList
//
//  Created by Сергей on 05.03.17.
//  Copyright © 2017 Сергей. All rights reserved.
//

import Foundation
import CoreData


extension Task {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Task> {
        return NSFetchRequest<Task>(entityName: "Task");
    }

    @NSManaged public var header: String?
    @NSManaged public var body: String?
    @NSManaged public var create: NSDate?
    @NSManaged public var event: NSDate?
    @NSManaged public var user: User?
    @NSManaged public var tags: Set<Tag>?
    @NSManaged public var status: TaskStatus?
    @NSManaged public var type: TaskType?

}

// MARK: Generated accessors for tags
extension Task {

    @objc(addTagsObject:)
    @NSManaged public func addToTags(_ value: Tag)

    @objc(removeTagsObject:)
    @NSManaged public func removeFromTags(_ value: Tag)

    @objc(addTags:)
    @NSManaged public func addToTags(_ values: NSSet)

    @objc(removeTags:)
    @NSManaged public func removeFromTags(_ values: NSSet)

}
