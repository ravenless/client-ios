import Foundation
import Gloss

struct DBTaskTag: Glossy {
    
    var id: Int?
    var name: String?
    
    init?(json: JSON) {
        id = "id" <~~ json
        name = "name" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "name" ~~> name
            ])
    }
}
