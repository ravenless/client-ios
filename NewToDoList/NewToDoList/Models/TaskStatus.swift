import Foundation
import Gloss

struct TaskStatus: Glossy {
    
    var id: Int?
    var name: String?
    
    init?(json: JSON) {
        id = "id" <~~ json
        name = "name" <~~ json
    }
    
    func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "name" ~~> name
            ])
    }
    
    init(id: Int?, name: String?) {
        self.id = id
        self.name = name
    }
}
