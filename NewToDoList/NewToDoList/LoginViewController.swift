import UIKit
import Material

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var windowLogin: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    fileprivate var emailField: ErrorTextField!
    fileprivate var passwordField: MainTextField!
    fileprivate var loginButton: RaisedButton!
    fileprivate var layer: Layer!
    
    var correctEmailField = false
    var correctPasswordField = false
    
    var constHeight: CGFloat!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        constHeight = CGFloat(40 + ((windowLogin.height - 250) / 5))
        prepareWindowsField()
        prepareEmailField()
        preparePasswordField()
        prepareLoginButton()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let field = textField as? ErrorTextField {
            if !field.isValidEmail() {
                field.isErrorRevealed = true
                correctEmailField = false
            }
        } else if let field = textField as? MainTextField {
            if !field.isValidPassword() {
                field.detailColor = Color.red
                field.correctField = false
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let field = textField as? ErrorTextField {
            field.isErrorRevealed = false
            correctEmailField = true
        } else if let field = textField as? MainTextField {
            field.detailColor = Color.grey.lighten1
            field.correctField = true
        }
    }
    
    func pressLoginButton(button: UIButton) {
        textFieldDidEndEditing(emailField)
        textFieldDidEndEditing(passwordField)
        if correctEmailField && passwordField.correctField {
            blockWindowLogin()
            ToDoClient.instance.authorization(email: emailField.text!.lowercased(), password: passwordField.text!) { result in
                if result.result {
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "logToMain", sender: nil)
                    }
                } else {
                    let messageError = UIAlertController(title: "Ошибка авторизации", message: result.message, preferredStyle: .actionSheet)
                    let actionOk = UIAlertAction(title: "Ок", style: .default)
                    messageError.addAction(actionOk)
                    DispatchQueue.main.async {
                        self.blockWindowLogin()
                        self.present(messageError, animated: true)
                    }
                }
            }
        } else {
            let messageError = UIAlertController(title: "Ошибка", message: "Не корректное заполнение полей", preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "Ок", style: .default)
            messageError.addAction(actionOk)
            self.present(messageError, animated: true)
        }
    }
    
    func blockWindowLogin() {
        titleLabel.text = loginButton.isEnabled ? "Подключение..." : "Авторизация"
        loginButton.isEnabled = loginButton.isEnabled ? false : true
        emailField.isEnabled = emailField.isEnabled ? false : true
        passwordField.isEnabled  = passwordField.isEnabled ? false : true
    }
}


extension LoginViewController {
    
    fileprivate func prepareWindowsField() {
        windowLogin.layer.cornerRadius = 10
        windowLogin.backgroundColor = Color.grey.lighten5
    }
    
    fileprivate func prepareEmailField() {
        
        emailField = ErrorTextField()
        emailField.placeholder = "Email"
        emailField.detail = "Ошибка, не корректный email"
        emailField.clearButtonMode = .whileEditing
        emailField.tag = 1
        emailField.delegate = self
        let imageEmail = UIImageView()
        imageEmail.image = #imageLiteral(resourceName: "Email")
        emailField.leftView = imageEmail
        windowLogin.layout(emailField).top(titleLabel.height + constHeight).left(20).right(20)
    }
    
    fileprivate func preparePasswordField() {
        
        passwordField = MainTextField()
        passwordField.placeholder = "Пароль"
        passwordField.detail = "Пароль не менее 5 символов"
        passwordField.detailColor = Color.grey.lighten1
        passwordField.clearButtonMode = .whileEditing
        passwordField.delegate = self
        let imageKey = UIImageView()
        imageKey.image = #imageLiteral(resourceName: "Key")
        passwordField.leftView = imageKey
        let visibility = UIImageView()
        visibility.image = #imageLiteral(resourceName: "Visibility")
        passwordField.rightView = visibility
        passwordField.isVisibilityIconButtonEnabled = true

        
        passwordField.visibilityIconButton?.tintColor = Color.green.base.withAlphaComponent(passwordField.isSecureTextEntry ? 0.6 : 1)
        windowLogin.layout(passwordField).top(titleLabel.height + emailField.height + constHeight * 2).left(20).right(20)
    }
    
    fileprivate func prepareLoginButton() {
        loginButton = RaisedButton(title: "Войти", titleColor: Color.blue.base)
        loginButton.backgroundColor = Color.grey.lighten3
        loginButton.addTarget(self, action: #selector(pressLoginButton(button:)), for: .touchUpInside)
        windowLogin.layout(loginButton).top(titleLabel.height + emailField.height * 2 + constHeight * 3 - 10).left(20).right(20)
    }
}
