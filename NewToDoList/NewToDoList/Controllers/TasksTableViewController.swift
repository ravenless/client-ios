import UIKit

class TasksTableViewController: UITableViewController {
    
    let dateFormatterFromJSON = DateFormatter()
    let dateFormatterToUI = DateFormatter()
    var dateObj: Date?

    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatterFromJSON.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
        dateFormatterToUI.dateFormat = "В HH:mm:ss dd числа"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let tasks = mainUser!.newTask {
            return tasks.count
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as! TaskTableViewCell
        cell.titleTaskLabel.text = mainUser?.newTask?[indexPath.row].header
        if let event = mainUser?.newTask?[indexPath.row].event {
            if let dateObj = dateFormatterFromJSON.date(from: event) {
                cell.eventDateLabel.text = dateFormatterToUI.string(from: dateObj)
            }
        } else {
            cell.calendarImage.image = #imageLiteral(resourceName: "CalendarFree")
            cell.eventDateLabel.text = ""
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let deleteTask = mainUser!.newTask?[indexPath.row] {
                deleteTask.status = deleteTaskStatus
                ToDoClient.instance.updateTask(task: deleteTask) { result in
                    if result.result {
                        print("Task success delete")
                    } else {
                        print("Delete error: \(result.message)")
                    }
                }
            }
            mainUser!.newTask!.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "listTaskToEditTask", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let controller = segue.destination as? AddTaskTableViewController, let index = sender as? Int else { return }
        controller.initAllObject(index)
        controller.typeEvent = .update
        controller.title = "Редактирование"
        controller.titleField.text = mainUser!.newTask[index].header
        
    }
    
    @IBAction func clickAddTask(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "listTaskToEditTask", sender: nil)
    }
    

}
