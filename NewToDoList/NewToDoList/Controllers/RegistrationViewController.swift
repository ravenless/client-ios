import UIKit
import Material

class RegistrationViewController: UIViewController, UITextFieldDelegate {
    
    fileprivate var emailField: ErrorTextField!
    fileprivate var loginField: MainTextField!
    fileprivate var passwordField: MainTextField!
    fileprivate var nameField: MainTextField!
    fileprivate var registrationButton: RaisedButton!

    var currectEmailField = false
    
    var constHeight: CGFloat!

    @IBOutlet weak var windowRegistration: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        constHeight = CGFloat(40 + ((windowRegistration.height - 390) / 5))
        prepareWindowRegistration()
        prepareNameField()
        prepareEmailField()
        prepareLoginField()
        preparePasswordField()
        prepareRegistrationButton()
    }
    
    func pressRegistrationButton(button: UIButton) {
        textFieldDidEndEditing(nameField)
        textFieldDidEndEditing(emailField)
        textFieldDidEndEditing(loginField)
        textFieldDidEndEditing(passwordField)
        if nameField.correctField && currectEmailField && loginField.correctField && passwordField.correctField {
            blockWindowRegistration()
            let tempUser = User(login: loginField.text!, password: passwordField.text!, name: nameField.text!, email: emailField.text!)
            ToDoClient.instance.registration(user: tempUser) { result in
                if result.result {
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "regToMain", sender: nil)
                    }
                } else {
                    let messageError = UIAlertController(title: "Ошибка регистрации", message: result.1, preferredStyle: .actionSheet)
                    let actionOk = UIAlertAction(title: "Ок", style: .default)
                    messageError.addAction(actionOk)
                    DispatchQueue.main.async {
                        self.blockWindowRegistration()
                        self.present(messageError, animated: true)
                    }
                }
            }
        } else {
            let messageError = UIAlertController(title: "Ошибка", message: "Не корректное заполнение полей", preferredStyle: .alert)
            let alertOk = UIAlertAction(title: "Ок", style: .default)
            messageError.addAction(alertOk)
            self.present(messageError, animated: true)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let field = textField as? ErrorTextField {
            if !field.isValidEmail() {
                field.isErrorRevealed = true
                currectEmailField = false
            }
        } else if let field = textField as? MainTextField {
            if field.tag == 1 && !field.isValidPassword(){
                field.detailColor = Color.red
                field.correctField = false
            } else if field.text!.isEmpty {
                field.detailLabel.isHidden = false
                field.correctField = false
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let field = textField as? ErrorTextField {
            field.isErrorRevealed = false
            currectEmailField = true
        } else if let field = textField as? MainTextField {
            if field.tag == 1 {
                field.detailColor = Color.grey.lighten1
                field.correctField = true
            } else {
                field.detailLabel.isHidden = true
                field.correctField = true
            }
        }
    }
    
    func blockWindowRegistration() {
        titleLabel.text = nameField.isEnabled ? "Регистрируемся..." : "Регистрация"
        nameField.isEnabled  = nameField.isEnabled ? false: true
        emailField.isEnabled = emailField.isEnabled ? false: true
        passwordField.isEnabled = passwordField.isEnabled ? false: true
        nameField.isEnabled = nameField.isEnabled ? false : true
        registrationButton.isEnabled = registrationButton.isEnabled ? false : true
    }
}

extension RegistrationViewController {
    fileprivate func prepareWindowRegistration() {
        windowRegistration.layer.cornerRadius = 10
        windowRegistration.backgroundColor = Color.grey.lighten5
    }
    
    fileprivate func prepareNameField() {
        nameField = MainTextField()
        nameField.placeholder = "Имя"
        nameField.clearButtonMode = .whileEditing
        nameField.detail = "Поле пустое"
        nameField.detailColor = Color.red
        nameField.detailLabel.isHidden = true
        nameField.delegate = self
        windowRegistration.layout(nameField).top(titleLabel.height + constHeight).left(20).right(20)
    }
    
    fileprivate func prepareEmailField() {
        emailField = ErrorTextField()
        emailField.placeholder = "Email"
        emailField.detail = "Ошибка, не корректный email"
        emailField.clearButtonMode = .whileEditing
        emailField.tag = 1
        emailField.delegate = self
        let imageEmail = UIImageView()
        imageEmail.image = #imageLiteral(resourceName: "Email")
        emailField.leftView = imageEmail
        windowRegistration.layout(emailField).top(titleLabel.height + nameField.height + constHeight * 2).left(20).right(20)
    }
    
    fileprivate func prepareLoginField() {
        loginField = MainTextField()
        loginField.placeholder = "Логин"
        loginField.detail = "Поле пустое"
        loginField.detailColor = Color.red
        loginField.detailLabel.isHidden = true
        loginField.delegate = self
        loginField.clearButtonMode = .whileEditing
        let imageAccount = UIImageView()
        imageAccount.image = #imageLiteral(resourceName: "Account")
        loginField.leftView = imageAccount
        windowRegistration.layout(loginField).top(titleLabel.height + nameField.height * 2 + constHeight * 3).left(20).right(20)
    }
    
    fileprivate func preparePasswordField() {
        passwordField = MainTextField()
        passwordField.placeholder = "Пароль"
        passwordField.detail = "Пароль не менее 5 символов"
        passwordField.clearButtonMode = .whileEditing
        passwordField.isVisibilityIconButtonEnabled = true
        passwordField.detailColor = Color.grey.lighten1
        passwordField.tag = 1
        passwordField.delegate = self
        let imageKey = UIImageView()
        imageKey.image = #imageLiteral(resourceName: "Key")
        passwordField.leftView = imageKey
        windowRegistration.layout(passwordField).top(titleLabel.height + nameField.height * 3 + constHeight * 4).left(20).right(20)
    }
    
    fileprivate func prepareRegistrationButton() {
        registrationButton = RaisedButton(title: "Зарегистрироваться", titleColor: Color.blue.base)
        registrationButton.backgroundColor = Color.grey.lighten3
        registrationButton.addTarget(self, action: #selector(pressRegistrationButton(button:)), for: .touchUpInside)
        windowRegistration.layout(registrationButton).top(titleLabel.height + nameField.height * 4 + constHeight * 5 - 10).left(20).right(20)
    }
    
}
