import UIKit
import Material

class AddTaskTableViewController: UITableViewController {
    
    var titleField: TextField!
    fileprivate var headerLabel: UILabel!
    var eventCalendar: UIDatePicker!
    fileprivate var calendarLabel: UILabel!
    var calendarSwitch: UISwitch!
    fileprivate var tagLabel: UILabel!
    fileprivate var createTaskButton: RaisedButton!
    var tags: [TaskTag]!
    
    var typeEvent: TypeTaskEvent = .insert
    var indexUpdateTask: Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addTaskCell", for: indexPath) as! AddTaskTableViewCell
        switch indexPath.row {
        case 0:
            prepareTitleField(cell)
        case 1:
            prepareCalendarSwitch(cell)
        case 2:
            prepareEventCalendar(cell)
        case 3:
            prepareTagLabel(cell)
        case 4:
            prepareCreateTaskButton(cell)
        default:
            print("Not found")
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 1,3,4:
            return 60
        case 2:
            return 180
        default:
            return 100
        }
    }
}

extension AddTaskTableViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField as? TextField {
            if text.tag == 1 {
                if string.isEmpty && text.text!.characters.count > 0 {
                    text.detail = "\(text.text!.characters.count - 1)/50"
                    if text.text!.characters.count - 1 == 0 {
                        text.detailColor = UIColor.red
                    }
                } else if string != "\n" {
                    if text.text!.characters.count < 50 {
                        text.detail = "\(text.text!.characters.count + 1)/50"
                        text.detailColor = UIColor.gray
                    } else {
                        return false
                    }
                }
            }
        }
        return true
    }
    
    func preparetHeaderLabel(_ cell: AddTaskTableViewCell) {
        headerLabel = headerLabel ?? UILabel()
        headerLabel.text = "Создать задачу"
        headerLabel.textAlignment = .center
        cell.mainTableView.layout(headerLabel).top(30).right(20).left(20)
    }
    
    func prepareTitleField(_ cell: AddTaskTableViewCell) {
        titleField = titleField ?? TextField()
        
        titleField.tag = 1
        titleField.placeholder = "Что напомнить?"
        titleField.detail = "\(titleField.text!.characters.count)/50"
        titleField.detailColor = UIColor.red
        titleField.clearButtonMode = .whileEditing
        titleField.delegate = self
        cell.mainTableView.layout(titleField).top(30).right(20).left(20)
    }
    
    func prepareCalendarSwitch(_ cell: AddTaskTableViewCell) {
        calendarLabel = calendarLabel ?? UILabel()
        calendarSwitch = calendarSwitch ?? UISwitch()
        
        calendarLabel.text = "Напомнить: "
        
        calendarSwitch.isOn = false
        calendarSwitch.isEnabled = false
        
        cell.mainTableView.layout(calendarSwitch).top(20).right(20)
        cell.mainTableView.layout(calendarLabel).top(20).right(40 + calendarSwitch.width).left(20)
    }
    
    func prepareEventCalendar(_ cell: AddTaskTableViewCell) {
        eventCalendar = eventCalendar ?? UIDatePicker()
        eventCalendar.minimumDate = Date()
        eventCalendar.minuteInterval = 1
        eventCalendar.datePickerMode = .dateAndTime
        eventCalendar.locale = Locale(identifier: "ru")
        eventCalendar.isEnabled = calendarSwitch.isOn
        cell.mainTableView.layout(eventCalendar).top(0).bottom(0).right(0).left(0)
    }
    
    func prepareTagLabel(_ cell: AddTaskTableViewCell) {
        tags = tags ?? [TaskTag]()
        tagLabel = tagLabel ?? UILabel()
        tagLabel.text = "Тэги"
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .default
        cell.mainTableView.layout(tagLabel).top(20).bottom(20).right(20).left(20)
    }
    
    func prepareCreateTaskButton(_ cell: AddTaskTableViewCell) {
        createTaskButton = createTaskButton ?? RaisedButton(title: "Создать задачу", titleColor: .white)
        createTaskButton.backgroundColor = tdlBlue
        createTaskButton.addTarget(self, action: #selector(pressCreateTask), for: .touchUpInside)
        cell.mainTableView.layout(createTaskButton).top(20).bottom(0).right(40).left(40)
    }
    
    @objc
    func pressCreateTask() {
        if !titleField.text!.isEmpty {
            var eventDate: String?
            if calendarSwitch.isOn {
                let formatDate = DateFormatter()
                formatDate.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
                eventDate = formatDate.string(from: eventCalendar.date)
            }
            
            let task = Task(event: eventDate, header: titleField.text, body: "", type: privateTaskType, status: newTaskStatus, tags: tags)
            
            switch typeEvent {
            case .insert:
                ToDoClient.instance.addTask(task: task) { result in
                    if result.result {
                        print("Задача успешно создана")
                        if mainUser!.newTask == nil {
                            mainUser!.newTask = [Task]()
                            mainUser!.newTask!.append(task)
                        } else {
                            mainUser!.newTask!.append(task)
                        }
                        let alert = UIAlertController(title: "Задача создана", message: "Ваша задача успешно добавлена", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ок", style: .default, handler: nil)
                        alert.addAction(alertAction)
                        DispatchQueue.main.async {
                            self.clearField()
                            self.present(alert, animated: true, completion: nil)
                        }
                    } else {
                        print("Задача упала с ошибкой")
                        let alert = UIAlertController(title: "Ошибка", message: result.message, preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ок", style: .default, handler: nil)
                        alert.addAction(alertAction)
                        DispatchQueue.main.async {
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            case .update:
                ToDoClient.instance.updateTask(task: task) { result in
                    if result.result {
                        print("Задача успешно изменена")
                        mainUser?.newTask[self.indexUpdateTask!] = task
                        let alert = UIAlertController(title: "Задача обновлена", message: "Ваша задача успешно обновлена", preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ок", style: .default, handler: nil)
                        alert.addAction(alertAction)
                        DispatchQueue.main.async {
                            self.clearField()
                            self.present(alert, animated: true, completion: nil)
                        }
                    } else {
                        print("Задача упала с ошибкой")
                        let alert = UIAlertController(title: "Ошибка", message: result.message, preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: "Ок", style: .default, handler: nil)
                        alert.addAction(alertAction)
                        DispatchQueue.main.async {
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            }
        } else {
            let alert = UIAlertController(title: "Ошибка", message: "Не заполнена цель задачи.", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "Ок", style: .default, handler: nil)
            alert.addAction(alertAction)
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func clearField() {
        titleField.text = ""
        eventCalendar.date = Date()
        tags = [TaskTag]()
    }
    
    func initAllObject(_ index: Int) {
        indexUpdateTask = index
        titleField = titleField ?? TextField()
        headerLabel = headerLabel ?? UILabel()
        eventCalendar = eventCalendar ?? UIDatePicker()
        calendarLabel = calendarLabel ?? UILabel()
        calendarSwitch = calendarSwitch ?? UISwitch()
        tags = tags ?? [TaskTag]()
        createTaskButton = createTaskButton ?? RaisedButton(title: "Изменить", titleColor: .white)
    }
}
