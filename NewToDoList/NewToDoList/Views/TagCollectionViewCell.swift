//
//  TagCollectionViewCell.swift
//  NewToDoList
//
//  Created by Сергей on 19.03.17.
//  Copyright © 2017 Сергей. All rights reserved.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tagLabel: UILabel!
}
