//
//  TextField+Check.swift
//  NewToDoList
//
//  Created by Сергей on 08.03.17.
//  Copyright © 2017 Сергей. All rights reserved.
//

import Foundation
import Material

extension TextField {
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text!)
    }
    
    func isValidPassword() -> Bool {
        let passRegEx = "[A-Z0-9a-z._/\\*&^%$#@]{5,}"
        let passTest = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        return passTest.evaluate(with: self.text!)
    }
}
