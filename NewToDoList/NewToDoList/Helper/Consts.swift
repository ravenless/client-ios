import Foundation

var newTaskStatus: TaskStatus {
get {
    return TaskStatus(id: 1, name: "Новая")
}
}

var successTaskStatus: TaskStatus {
get {
    return TaskStatus(id: 2, name: "Выполненая")
}
}

var deleteTaskStatus: TaskStatus {
get {
    return TaskStatus(id: 3, name: "Удаленная")
}
}

var privateTaskType: TaskType {
get {
    return TaskType(id: 1, name: "Личная")
}
}
